# -*- coding: utf-8 -*-

__author__ = 'Eirik Aabøe & Joakim Tveit Husefest'

from flask import Flask
from flask import request
from flask.ext.jsonpify import jsonify
from geojson import MultiLineString
import geojson
import networkx as nx
import numpy as np
import psycopg2

app = Flask(__name__)

SOURCE_LON = 10.827489770262536
SOURCE_LAT = 59.656442077770549
TARGET_LON = 10.759221616738442
TARGET_LAT = 59.742822124512166
SOURCE_ID = 33552
TARGET_ID = 33556

@app.route('/jsonfactory')
def shortest_path(source_lon=SOURCE_LON, source_lat=SOURCE_LAT, target_lon=TARGET_LON, target_lat=TARGET_LAT):
    source_lon = float(request.args.get('sourcelon', source_lon))
    source_lat = float(request.args.get('sourcelat', source_lat))
    target_lon = float(request.args.get('targetlon', target_lon))
    target_lat = float(request.args.get('targetlat', target_lat))

    G = nx.read_shp('edge_data.shp')
    sg = list(nx.connected_component_subgraphs(G.to_undirected()))[0]
    nodes = np.array(sg.nodes())
    # source = tuple(nodes[0])
    # target = tuple(nodes[1])

    source = tuple([source_lon, source_lat])
    target = tuple([target_lon, target_lat])
    path = nx.dijkstra_path(sg,
                            source=source,
                            target=target,
                            weight='distance')
    path = tuple([path])
    json = jsonify(get_feature(MultiLineString(path)))
    return json

@app.route('/pgRouting')
def sql(source_id = SOURCE_ID, target_id = TARGET_ID):
    source_id = (request.args.get('sourceid', source_id))
    target_id = (request.args.get('targetid', target_id))

    try:
        conn = psycopg2.connect(dbname='Elveg', user='postgres',
        password='<------>', host='localhost', port=5432)
    except ValueError:
        print('database ikke tilkoblet')

    sql = ("""with rute as (SELECT *
       FROM topo.edge_data JOIN (SELECT * FROM pgr_dijkstra('SELECT edge_id AS id, start_node::int4 AS source,
       end_node::int4 AS target, length::double precision as cost FROM topo.edge_data', {}, {}, false, false))
       AS route ON topo.edge_data.edge_id = route.id2)
       select st_asgeojson(st_union(geom)) from rute;""").format(source_id, target_id)

    cursor = conn.cursor()
    #cursor.execute(sql)
    cursor.execute(sql)
    rows = cursor.fetchall()
    json = (str(rows)[3:][:-4])
    f = open('route.geojson', 'w')
    f.write(json)
    f.close()
    return json



@app.route('/')
def index():
    html = """<h1>Welcome to the routePick geojson-factory!</h1>
    <p>To use it, add /jsonfactory and paramters to this url!</p>
    <p>The parameters are:</p>
    <table>
    <tr>
    <th>Parameter</th>
    <th>Description</th> 
    </tr>
    <tr>
    <td>sourcelon</td>
    <td>Longitude of the source</td>
    </tr>
    <tr>
    <td>sourcelat</td>
    <td>Latitude of the source</td>
    </tr>
    <tr>
    <td>targetlon</td>
    <td>Longitude of the target</td>
    </tr>
    <tr>
    <td>targetlat</td>
    <td>Latitude of the target</td>
    </tr>"""
    return html


def get_feature(figure):
    return geojson.Feature(geometry=figure)


if __name__ == '__main__':
    app.run(debug=True)
