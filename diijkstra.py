__author__ = 'eirikaa'

import psycopg2

conn = psycopg2.connect(dbname='topologi_test', user='postgres', password='<------>', host='localhost', port=5432)
#conn = psycopg2.connect(dbname='gdbstud', user='gdbstud', password='postgis300', host='gis.umb.no', port=5436)

def list_data():
    distances = {}

    lengde_liste = []
    edge_id_liste = []
    start_node_liste = []
    end_node_liste = []

    for i in range(1,9):
        dist = ("select length from conf.edge_data where edge_id = {}".format(i))
        edge_id = ("select edge_id from conf.edge_data where edge_id = {}".format(i))
        start_node = ("select start_node from conf.edge_data where edge_id = {}".format(i))
        end_node = ("select end_node from conf.edge_data where edge_id = {}".format(i))



        for j in [dist,edge_id, start_node, end_node]:

            queryobj = conn.cursor()
            queryobj.execute(j)
            result = queryobj.fetchall()
            if j == dist:
                lengde_liste.append(result)
            elif j == edge_id:
                edge_id_liste.append(result)
            elif j == start_node:
                start_node_liste.append(result)
            else:
                end_node_liste.append(result)
    return lengde_liste, edge_id_liste, start_node_liste, end_node_liste

def fix_data(lengde_liste, edge_id_liste, start_node_liste, end_node_liste):
    lengde = []
    edge_id = []
    start_node= []
    end_node = []

    for i in lengde_liste:
        for y in i:
            lengde.append(y[0])

    for i in edge_id_liste:
        for y in i:
            edge_id.append(y[0])

    for i in start_node_liste:
        for y in i:
            start_node.append(y[0])

    for i in end_node_liste:
        for y in i:
            end_node.append(y[0])

    return lengde, edge_id, start_node, end_node

def make_edge_dict(lengde, edge_id, start_node, end_node):
    attribute_table = {}
    for i in range(8):
        attribute_table[edge_id[i]] = [lengde[i], start_node[i], end_node[i]]
    return attribute_table

def diijkstra(attribute_table, start, end):
    route_dict = {}
    temp = []
    for j in range(1, len(attribute_table)+1):
        if attribute_table[j][1] == start or attribute_table[j][2] == start:
            temp.append(attribute_table[j])

    for i in range(len(temp)):
        route_dict[i+1] = temp[i]


    print route_dict
    for k in range(1, len(route_dict)):
        for j in range(1, len(attribute_table)+1):
            if (attribute_table[j][1] == route_dict[k][1] or attribute_table[j][2] == route_dict[k][1] or attribute_table[j][1] == route_dict[k][2] or attribute_table[j][2] == route_dict[k][2]):
                temp.append(attribute_table[j])




    temp2 = []
    for z in range(len(temp)):
        if temp[z] in route_dict.values():
            temp2.append(z)

    for f in reversed(temp2):
        temp.pop(f)
    for l in range(len(temp)):
        route_dict[l+1] = temp[l]
    print route_dict






if __name__ == '__main__':
    a,b,c,d = list_data()
    e,f,g,h = fix_data(a,b,c,d)
    i = make_edge_dict(e,f,g,h)
    print i
    j = diijkstra(i, 1, 0)
