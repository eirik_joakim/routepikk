var click = 0;
var coords = {};


var map = L.map('map').setView([59.665150577795885, 10.775130987167358], 12);
mapLink =
    '<a href="http://openstreetmap.org">OpenStreetMap</a>';
L.tileLayer(
    'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; ' + mapLink + ' Contributors',
    maxZoom: 18,
  }).addTo(map);
//var layer = new L.TileLayer.Kartverket('topo2graatone');
//layer.addTo(map);
//Eirik
var url = "http://localhost:8080/geoserver/Aas_topo/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=Aas_topo:node&outputFormat=text/javascript&format_options=callback:getJson";

 //Joakim
//var url = "http://localhost:8080/geoserver/route/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=route:node&maxFeatures=5000&outputFormat=text%2Fjavascript&format_options=callback:getJson";

$.ajax({
    jsonp: false,
    jsonpCallback: 'getJson',
    type: 'GET',
    url: url,
    async: false,
    dataType: 'jsonp',
    success: handleJson
});

function handleJson(data)
    {
        var WFSlayer = L.geoJson(data,
          {
            onEachFeature: onEachFeature});
            var markers = L.markerClusterGroup();
markers.addLayer(WFSlayer);
        map.addLayer(markers);
          }
    

function onEachFeature(feature, layer) {
    layer.on('click', function(e){
        click += 1;
        var coord = e.latlng;
        var str = feature.id;
        var id = str.slice(5,10)
        if (click === 1) {
            //removeLayer(geojsonLayer);
            coords.lat1 = coord['lat'].toString();
            coords.lon1 = coord['lng'].toString();
            str = feature.id;
            id1 = str.slice(5,10);
            
        } else if (click === 2) {
            coords.lat2 = coord['lat'].toString();
            coords.lon2 = coord['lng'].toString();
            str = feature.id;
            id2 = str.slice(5,10);
            
            var geojsonLayer = L.geoJson.ajax("http://localhost:5000/jsonfactory?sourcelon="+coords.lon1+"&sourcelat="+coords.lat1+"&targetlon="+coords.lon2+"&targetlat="+coords.lat2,{dataType:"jsonp"});
geojsonLayer.addTo(map);
            var pgRouting = L.geoJson.ajax("http://localhost:5000/pgRouting?sourceid="+id1+"&targetid="+id2);
            
            
        } 
        else{var myStyle = {"color": "#ff0000"};
             var pgRoutingLayer=L.geoJson.ajax("route.geojson",{style:myStyle});
             pgRoutingLayer.addTo(map);
             click = 0;}
    });
}



