# -*- coding: utf-8 -*-

__author__ = 'eirikaa'

import networkx as nx
import numpy as np
import geojson


def path():
    G = nx.read_shp('edge_data.shp')
    sg = list(nx.connected_component_subgraphs(G.to_undirected()))[0]
    nodes = np.array(sg.nodes())
    path = nx.shortest_path(sg,
                            source=tuple(nodes[0]),
                            target=tuple(nodes[1]),
                            weight='distance')
    path = tuple([path])
    return path

def get_feature(figure):
    return geojson.Feature(geometry=figure)

def get_json(feature):
    #return (geojson.dumps(feature, sort_keys=True))
    return ((feature))


# nx.write_shp(path,'C:/Users/eirikaa/Documents/NMBU/GMGI300/Øvinger/Postgis_topologi_prosjekt/kode/python/shp')__author__ = 'eirikaa'
