import psycopg2
from osgeo import ogr
import os
from flask.ext.jsonpify import jsonify
import geojson
from flask import Flask

app2 = Flask(__name__)

def sql():

    try:
        conn = psycopg2.connect(dbname='elveg', user='joakimtveithusefest',
        password='ytrebygda', host='localhost', port=5432)
    except ValueError:
        print('database ikke tilkoblet')

    sql = ("""with rute as (SELECT *
       FROM topotest.edge_data JOIN (SELECT * FROM pgr_dijkstra('SELECT edge_id AS id, start_node::int4 AS source,
       end_node::int4 AS target, length::double precision as cost FROM topotest.edge_data', 9, 250, false, false))
       AS route ON topotest.edge_data.edge_id = route.id2)
       select st_asgeojson(st_union(geom)) from rute;""")

    cursor = conn.cursor()
    #cursor.execute(sql)
    cursor.execute(sql)
    rows = cursor.fetchall()
    return rows


@app2.route('/')
def get_json():
    rows = sql()
    return jsonify(str(rows)[3:][:-4])

if __name__ == '__main__':
    app2.run(debug=True)

