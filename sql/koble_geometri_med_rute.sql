﻿with routeview as (SELECT * 
   FROM topotest.edge_data
   JOIN
   (SELECT * FROM pgr_dijkstra('
   SELECT edge_id AS id,
          start_node::int4 AS source, 
          end_node::int4 AS target, 
          length::double precision as cost
   FROM topotest.edge_data',
9,
250,
false,
false)) AS route
   ON
   topotest.edge_data.edge_id = route.id2)
   select ST_asgeojson(geom) from routeview;
